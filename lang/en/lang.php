<?php

return [
    'plugin' => [
        'name' => 'Static page revisions',
        'description' => 'Allows to manage static page revisions.',
    ],
    'tabs' => [
        'revisions' => 'Revisions',
    ],
    'fields' => [

    ],
    'permissions' => [
        'manage_page_revisions' => 'Manage page revisions',
    ],
    'actions' => [
        'preview_version' => 'Preview version',
        'revert_to_version' => 'Revert to version',
        'revert_to_version_success' => 'Page ":title" was reverted to version from :date.',
    ],
];
