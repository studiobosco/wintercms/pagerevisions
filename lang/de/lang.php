<?php

return [
    'plugin' => [
        'name' => 'Seitenversionen',
        'description' => 'Erlaubt die Verwaltung der Versionen von Seiten.',
    ],
    'tabs' => [
        'revisions' => 'Versionen',
    ],
    'fields' => [

    ],
    'permissions' => [
        'manage_page_revisions' => 'Seitenversionen verwalten',
    ],
    'actions' => [
        'preview_version' => 'Vorschau der Version',
        'revert_to_version' => 'Zur Version zurückkehren',
        'revert_to_version_success' => 'Seite ":title" wurde auf Version vom :date zurückgesetzt.',
    ],
];
