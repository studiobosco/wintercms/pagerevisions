<?php namespace StudioBosco\PageRevisions;

use App;
use Event;
use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * PageRevisions Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array Plugin dependencies
     */
    public $require = ['Winter.Pages'];

    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'studiobosco.pagerevisions::lang.plugin.name',
            'description' => 'studiobosco.pagerevisions::lang.plugin.description',
            'author'      => 'StudioBosco',
            'icon'        => 'icon-code-compare'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {
        if (!App::runningInBackend()) {
            return;
        }

        \Winter\Pages\Controllers\Index::extend(function ($controller) {
            $controller->addCss('/modules/cms/assets/css/themelogs/template-diff.css', 'core');
            $controller->addJs('/modules/cms/assets/vendor/jsdiff/diff.js', 'core');
            $controller->addJs('/modules/cms/assets/js/themelogs/template-diff.js', 'core');
        });

        Event::listen('backend.form.extendFields', function($formWidget) {
            if (
                $formWidget->isNested
                || !($formWidget->model instanceof \Winter\Pages\Classes\Page)
            ) {
                return;
            }

            $formWidget->addTabFields([
                '_revisions' => [
                    'tab' => 'studiobosco.pagerevisions::lang.tabs.revisions',
                    'type' => 'pagerevisions',
                    'permissions' => ['studiobosco.pagerevisions.manage_page_revisions'],
                ],
            ]);
        });
    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return [];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return [
            'studiobosco.pagerevisions.manage_page_revisions' => [
                'tab' => 'studiobosco.pagerevisions::lang.plugin.name',
                'label' => 'studiobosco.pagerevisions::lang.permissions.manage_page_revisions',
                'roles' => [UserRole::CODE_DEVELOPER, UserRole::CODE_PUBLISHER],
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return [];
    }

    public function registerFormWidgets()
    {
        return [
            'StudioBosco\PageRevisions\FormWidgets\PageRevisions' => 'pagerevisions',
        ];
    }
}
