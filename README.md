# Page Revisions Plugin

WinterCMS Plugin that allows to manage Revisions of static pages.

After installing enable [Theme Logging](https://wintercms.com/docs/v1.2/docs/cms/themes#theme-logging) and add the permission to manage page revisions to the admin role.
Now you should see a "Versions" Tab in all static pages.

If you have the [Page preview plugin](https://packagist.org/packages/studiobosco/wn-pagepreview-plugin) enabled it will show a version preview button that will open the preview of the version to the side.
