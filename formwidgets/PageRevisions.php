<?php namespace StudioBosco\PageRevisions\FormWidgets;

use Input;
use Lang;
use Flash;
use Ini;
use Session;
use Backend;
use Backend\Classes\FormWidgetBase;
use Winter\Storm\Database\Collection;

class PageRevisions extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'pagerevisions';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig();
        $this->loadAssets();
    }

    /**
     * @inheritDoc
     */
    protected function loadAssets()
    {
        $this->addCss('css/pagerevisions.css', 'studiobosco.pagerevisions');
        $this->addJs('js/pagerevisions.js', 'studiobosco.pagerevisions');
    }

    protected function localeDate($date, $format, $locale = null)
    {
        $date = new \DateTime($date);
        $locale = $locale ?? Lang::getLocale();
        $formatter = new \IntlDateFormatter(
            $locale,
            \IntlDateFormatter::NONE,
            \IntlDateFormatter::NONE
        );
        $formatter->setPattern($format);
        return $formatter->format($date);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('pagerevisions');
    }

    /**
     * Prepares the list data
     */
    public function prepareVars()
    {
        $this->vars['history'] = $this->getHistory();
        $this->vars['pagePreviewEnabled'] = $this->isPagePreviewEnabled();
    }

    protected function getHistory()
    {
        $hasTranslatePlugin = \System\Classes\PluginManager::instance()->hasPlugin('Winter.Translate');
        if ($hasTranslatePlugin) {
            $translator = \Winter\Translate\Classes\Translator::instance();
            $defaultLocale = $translator->getDefaultLocale();
            $hasMultipleLocales = \Winter\Translate\Models\Locale::isAvailable();
        } else {
            $defaultLocale = null;
            $hasMultipleLocales = false;
        }
        if ($hasMultipleLocales) {
            $locales = \Winter\Translate\Models\Locale::listAvailable();
        } else {
            $locales = [];
        }

        $theme = \Cms\Classes\Theme::getActiveThemeCode();
        $themeLogs = new Collection();

        if ($hasTranslatePlugin && $hasMultipleLocales) {
            foreach($locales as $locale => $localeName) {
                if ($locale === $defaultLocale) {
                    $fileName = $this->model->getObjectTypeDirName() . '/' . $this->model->getFileName();
                } else {
                    $fileName = $this->model->getObjectTypeDirName() . '-' . $locale . '/' . $this->model->getFileName();
                }
                $records = \Cms\Models\ThemeLog::where('type', 'update')->where('theme', $theme)->where('template', $fileName)->get();

                // append locale to record
                foreach($records as $record) {
                    $record->locale = $locale;
                }

                $themeLogs = $themeLogs->concat($records);
            }
        } else {
            $fileName = $this->model->getObjectTypeDirName() . '/' . $this->model->getFileName();
            $records = \Cms\Models\ThemeLog::where('type', 'update')->where('theme', $theme)->where('template', $fileName)->get();
            $themeLogs = $themeLogs->concat($records);
        }

        $themeLogs = $themeLogs->sortByDesc('created_at');

        return $themeLogs;
    }

    public function getCurrentContent($locale = null)
    {
        $hasTranslatePlugin = \System\Classes\PluginManager::instance()->hasPlugin('Winter.Translate');

        if ($locale && $hasTranslatePlugin) {
            return $this->model->getAttributeTranslated('content', $locale);
        } else {
            return $this->model->content;
        }
    }

    protected function getUserName($userId)
    {
        $user = \Backend\Models\User::find($userId);

        return $user ? $user->first_name . ' ' . $user->last_name : '';
    }

    protected function isPagePreviewEnabled()
    {
        return \System\Classes\PluginManager::instance()->hasPlugin('StudioBosco.PagePreview') && \StudioBosco\PagePreview\Models\Settings::get('use_for_static_pages', false);
    }

    protected function parsePageContent(string $content): array
    {
        $content = preg_split('/^==$/', $content, 2);
        $content[0] = Ini::parse(rtrim($content[0], '=='));

        return $content;
    }

    public function onPreviewVersion()
    {
        $revisionId = Input::get('revision_id');
        $revision = \Cms\Models\ThemeLog::find($revisionId);

        if ($revision) {
            $oldContent = $this->parsePageContent($revision->old_content);
            $data = array_merge($oldContent[0], ['markup' => $oldContent[1] ?? '']);
            $previewData = array_merge($data, [
                'objectPath' => $this->model->getBaseFileName(),
            ]);

            Session::put('studiobosco.pagepreview.staticpage_preview_data', $previewData);
        }
    }

    public function onRevertToVersion()
    {
        $revisionId = Input::get('revision_id');
        $locale = Input::get('locale');
        $revision = \Cms\Models\ThemeLog::find($revisionId);

        if ($revision) {
            $hasTranslatePlugin = \System\Classes\PluginManager::instance()->hasPlugin('Winter.Translate');

            if ($hasTranslatePlugin) {
                $hasMultipleLocales = \Winter\Translate\Models\Locale::isAvailable();
            } else {
                $hasMultipleLocales = false;
            }

            $oldContent = $this->parsePageContent($revision->old_content);

            if ($locale && $hasTranslatePlugin && $hasMultipleLocales) {
                foreach($oldContent[0] as $key => $value) {
                    $this->model->setAttributeTranslated($key, $value, $locale);
                }
                $this->model->setAttributeTranslated('markup', $oldContent[1] ?? '', $locale);
            } else {
                foreach($oldContent[0] as $key => $value) {
                    $this->model->setAttribute($key, $value);
                }
                $this->model->markup = $oldContent[1] ?? '';
            }

            $this->model->save();

            Flash::success(trans('studiobosco.pagerevisions::lang.actions.revert_to_version_success', ['title' => $this->model->title, 'date' => $this->localeDate($revision->created_at, 'MM/dd/yyyy HH:mm:ss')]));

            return redirect(request()->fullUrl());
        }
    }
}
