function previewPageVersion(el, handler, url, revisionId, locale) {
  $(el).request(handler, {
    data: {
      revision_id: revisionId,
      locale: locale,
    },
    success: () => {
      openPagePreview(url, el, { noUpdate: true });
    },
  });
}
